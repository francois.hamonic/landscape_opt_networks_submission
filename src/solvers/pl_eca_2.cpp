#include "solvers/pl_eca_2.hpp"

#include "CglFlowCover.hpp"
#include "CglMixedIntegerRounding2.hpp"

// #define WITH_GUROBI
#ifdef WITH_GUROBI
#include "gurobi_c.h"
#endif

namespace Solvers::PL_ECA_2_Vars {
class PreprocessedDatas {
public:
    std::vector<MutableLandscape::Node> target_nodes;
    const MutableLandscape & landscape;
    const RestorationPlan<MutableLandscape> & plan;
    MutableLandscape::Graph::NodeMap<double> max_inflow_map;

    PreprocessedDatas(const MutableLandscape & landscape,
                      const RestorationPlan<MutableLandscape> & plan)
        : landscape(landscape)
        , plan(plan)
        , max_inflow_map(landscape.getNetwork()) {
        const MutableLandscape::Graph & graph = landscape.getNetwork();
        // target_nodes
        std::vector<MutableLandscape::Node> nodes;
        for(MutableLandscape::NodeIt u(graph); u != lemon::INVALID; ++u) {
            nodes.push_back(u);
            if(landscape.getQuality(u) == 0 && !plan.contains(u)) continue;
            target_nodes.push_back(u);
        }
        // max_inflow_map
        std::for_each(std::execution::par_unseq, nodes.begin(), nodes.end(),
                      [&](MutableLandscape::Node v) {
                          max_inflow_map[v] = max_flow_in(landscape, plan, v);
                      });
    }
    ~PreprocessedDatas() {}
};

class XVar : public OSI_Builder::VarType {
private:
    const MutableLandscape & _landscape;

public:
    XVar(const MutableLandscape & landscape)
        : VarType(lemon::countArcs(landscape.getNetwork()))
        , _landscape(landscape) {}
    int id(MutableLandscape::Arc a) const {
        const int id = _landscape.getNetwork().id(a);
        assert(id >= 0 && id < _number);
        return _offset + id;
    }
};
class RestoredXVar : public OSI_Builder::VarType {
public:
    RestoredXVar(const RestorationPlan<MutableLandscape> & plan)
        : VarType(plan.getNbArcRestorationElements()) {}
    int id(RestorationPlan<MutableLandscape>::ArcRestorationElement e) const {
        assert(e.id >= 0 && e.id < _number);
        return _offset + e.id;
    }
};
class FVar : public OSI_Builder::VarType {
public:
    FVar() : VarType(1) {}
    int id() const { return _offset; }
};
class RestoredFVar : public OSI_Builder::VarType {
public:
    RestoredFVar(const RestorationPlan<MutableLandscape> & plan)
        : VarType(plan.getNbNodeRestorationElements()) {}
    int id(RestorationPlan<MutableLandscape>::NodeRestorationElement e) const {
        assert(e.id >= 0 && e.id < _number);
        return _offset + e.id;
    }
};
class YVar : public OSI_Builder::VarType {
public:
    YVar(const RestorationPlan<MutableLandscape> & plan)
        : VarType(plan.getNbOptions(), 0, 1, true) {}
    int id(RestorationPlan<MutableLandscape>::Option option) const {
        const int id = option;
        assert(id >= 0 && id < _number);
        return _offset + id;
    }
};

class TVars {
public:
    XVar x;
    RestoredXVar restored_x;
    FVar f;
    RestoredFVar restored_f;
    TVars(const MutableLandscape & landscape,
          const RestorationPlan<MutableLandscape> & plan)
        : x(landscape), restored_x(plan), f(), restored_f(plan){};
};

class Variables {
public:
    MutableLandscape::Graph::NodeMap<TVars *> tvars;
    YVar y;
    const PreprocessedDatas & _pdatas;
    Variables(const MutableLandscape & landscape,
              const RestorationPlan<MutableLandscape> & plan,
              PreprocessedDatas & pdatas)
        : tvars(landscape.getNetwork(), nullptr), y(plan), _pdatas(pdatas) {
        for(MutableLandscape::Node v : pdatas.target_nodes)
            tvars[v] = new TVars(landscape, plan);
    }
    ~Variables() {
        for(MutableLandscape::Node v : _pdatas.target_nodes) delete tvars[v];
    }
    TVars & operator[](MutableLandscape::Node t) const { return *tvars[t]; }
};

void name_variables(OSI_Builder & solver, const MutableLandscape & landscape,
                    const RestorationPlan<MutableLandscape> & plan,
                    PreprocessedDatas & pdatas, const Variables & vars) {
    const MutableLandscape::Graph & graph = landscape.getNetwork();
    // XVar
    for(MutableLandscape::Node t : pdatas.target_nodes) {
        const TVars & tvars = vars[t];
        for(MutableLandscape::ArcIt a(graph); a != lemon::INVALID; ++a) {
            // solver->setColName(x_var->id(a), "x_t_" +
            // std::to_string(graph.id(v)) + "_a_" +
            // std::to_string(graph.id(a)));
            solver.setColName(
                tvars.x.id(a),
                "x_t_" + std::to_string(graph.id(t)) + "(" +
                    std::to_string(graph.id(t)) + ")_a_" +
                    std::to_string(graph.id(a)) + "(" +
                    std::to_string(graph.id(graph.source(a))) + "," +
                    std::to_string(graph.id(graph.target(a))) + ")");
            // RestoredXVar
            for(auto const & e : plan[a])
                solver.setColName(tvars.restored_x.id(e),
                                  "restored_x_t_" +
                                      std::to_string(graph.id(t)) + "_a_" +
                                      std::to_string(graph.id(a)) + "_" +
                                      std::to_string(e.option));
        }
    }
    // FVar
    for(MutableLandscape::Node t : pdatas.target_nodes)
        solver.setColName(vars[t].f.id(), "f_t_" + std::to_string(graph.id(t)));
    // RestoredFVar
    for(MutableLandscape::Node t : pdatas.target_nodes)
        for(auto const & e : plan[t])
            solver.setColName(vars[t].restored_f.id(e),
                              "restored_f_t_" + std::to_string(graph.id(t)) +
                                  "_" + std::to_string(e.option));
    // YVar
    for(const RestorationPlan<MutableLandscape>::Option i : plan.options())
        solver.setColName(vars.y.id(i), "y_" + std::to_string(i));
}
}  // namespace Solvers::PL_ECA_2_Vars

using namespace Solvers::PL_ECA_2_Vars;

void insert_variables(OSI_Builder & solver_builder, Variables & vars,
                      PreprocessedDatas & pdatas) {
    for(MutableLandscape::Node t : pdatas.target_nodes) {
        solver_builder.addVarType(&vars[t].x);
        solver_builder.addVarType(&vars[t].restored_x);
        solver_builder.addVarType(&vars[t].f);
        solver_builder.addVarType(&vars[t].restored_f);
    }
    solver_builder.addVarType(&vars.y);
    solver_builder.init();
}

void fill_solver(OSI_Builder & solver_builder,
                 const MutableLandscape & landscape,
                 const RestorationPlan<MutableLandscape> & plan, const double B,
                 Variables & vars, PreprocessedDatas & pdatas) {
    const MutableLandscape::Graph & graph = landscape.getNetwork();
    auto M_x_const = [&](MutableLandscape::Arc a) {
        return pdatas.max_inflow_map[graph.source(a)];
    };
    auto M_f_const = [&](MutableLandscape::Node t) {
        return pdatas.max_inflow_map[t];
    };
    ////////////////////////////////////////////////////////////////////////
    // Columns : Objective
    ////////////////////
    for(MutableLandscape::Node t : pdatas.target_nodes) {
        // sum w(t) * f_t
        const int f_t = vars[t].f.id();
        solver_builder.setObjective(f_t, landscape.getQuality(t));
        for(auto const & e : plan[t]) {
            const int restored_f_t = vars[t].restored_f.id(e);
            solver_builder.setObjective(restored_f_t, e.quality_gain);
        }
    }
    ////////////////////////////////////////////////////////////////////////
    // Rows : Constraints
    ////////////////////
    for(MutableLandscape::Node t : pdatas.target_nodes) {
        const TVars & tvars = vars[t];
        const int f_t = tvars.f.id();
        // out_flow(u) - in_flow(u) <= w(u)
        for(MutableLandscape::NodeIt u(graph); u != lemon::INVALID; ++u) {
            // out flow
            for(MutableLandscape::Graph::OutArcIt b(graph, u);
                b != lemon::INVALID; ++b) {
                const int x_tb = tvars.x.id(b);
                solver_builder.buffEntry(x_tb, 1);
                for(auto const & e : plan[b]) {
                    const int restored_x_t_b = tvars.restored_x.id(e);
                    solver_builder.buffEntry(restored_x_t_b, 1);
                }
            }
            // in flow
            for(MutableLandscape::Graph::InArcIt a(graph, u);
                a != lemon::INVALID; ++a) {
                const int x_ta = tvars.x.id(a);
                solver_builder.buffEntry(x_ta, -landscape.getProbability(a));
                for(auto const & e : plan[a]) {
                    const int degraded_x_t_a = tvars.restored_x.id(e);
                    solver_builder.buffEntry(degraded_x_t_a,
                                             -e.restored_probability);
                }
            }
            // optional injected flow
            for(auto const & e : plan[u]) {
                const int y_u = vars.y.id(e.option);
                solver_builder.buffEntry(y_u, -e.quality_gain);
            }
            // optimisation variable
            if(u == t) solver_builder.buffEntry(f_t, 1);
            // injected flow
            solver_builder.pushRow(-OSI_Builder::INFTY,
                                   landscape.getQuality(u));
        }
        // restored_x_a < y_i * M
        for(MutableLandscape::ArcIt a(graph); a != lemon::INVALID; ++a) {
            for(auto const & e : plan[a]) {
                const int y_i = vars.y.id(e.option);
                const int x_ta = tvars.restored_x.id(e);
                solver_builder.buffEntry(y_i, M_x_const(a));
                solver_builder.buffEntry(x_ta, -1);
                solver_builder.pushRow(0, OSI_Builder::INFTY);
            }
        }
        // restored_f_t <= f_t
        // restored_f_t <= y_i * M
        for(const auto & e : plan[t]) {
            const int y_i = vars.y.id(e.option);
            const int restored_f_t = tvars.restored_f.id(e);
            solver_builder.buffEntry(f_t, 1);
            solver_builder.buffEntry(restored_f_t, -1);
            solver_builder.pushRow(0, OSI_Builder::INFTY);

            solver_builder.buffEntry(y_i, M_f_const(t));
            solver_builder.buffEntry(restored_f_t, -1);
            solver_builder.pushRow(0, OSI_Builder::INFTY);
        }
    }
    ////////////////////
    // sum y_i < B
    for(const RestorationPlan<MutableLandscape>::Option i : plan.options()) {
        const int y_i = vars.y.id(i);
        solver_builder.buffEntry(y_i, plan.getCost(i));
    }
    solver_builder.pushRow(0, B);
}

Solution Solvers::PL_ECA_2::solve(
    const MutableLandscape & landscape,
    const RestorationPlan<MutableLandscape> & plan, const double B) const {
    Solution solution(landscape, plan);
    const int log_level = params.at("log")->getInt();
    const int timeout = params.at("timeout")->getInt();
    (void)timeout;  // pas bien
    const bool relaxed = params.at("relaxed")->getBool();
    Chrono chrono;
    if(log_level > 0)
        std::cout << name() << ": Start preprocessing" << std::endl;
    PreprocessedDatas preprocessed_datas(landscape, plan);
    solution.preprocessing_time = chrono.lapTimeMs();
    OSI_Builder solver_builder = OSI_Builder();
    Variables vars(landscape, plan, preprocessed_datas);
    insert_variables(solver_builder, vars, preprocessed_datas);
    if(log_level > 0) {
        std::cout << name()
                  << ": Complete preprocessing : " << chrono.lapTimeMs()
                  << " ms" << std::endl;
        std::cout << name()
                  << ": Start filling solver : " << solver_builder.getNbVars()
                  << " variables" << std::endl;
    }
    fill_solver(solver_builder, landscape, plan, B, vars, preprocessed_datas);
#ifndef WITH_GUROBI
    OsiSolverInterface * solver =
        solver_builder.buildSolver<OsiClpSolverInterface>(OSI_Builder::MAX);
    if(log_level <= 1) solver->setHintParam(OsiDoReducePrint);
    if(log_level >= 1) {
        if(log_level >= 3) {
            name_variables(solver_builder, landscape, plan, preprocessed_datas,
                           vars);
            OsiClpSolverInterface * solver_clp =
                solver_builder.buildSolver<OsiClpSolverInterface>(
                    OSI_Builder::MAX);
            solver_clp->writeLp("pl_eca_2");
            delete solver_clp;
            std::cout << name() << ": LP printed to 'pl_eca_2.lp'" << std::endl;
        }
        std::cout << name() << ": Complete filling solver : "
                  << solver_builder.getNbConstraints() << " constraints and "
                  << solver_builder.getNbElems() << " entries in "
                  << chrono.lapTimeMs() << " ms" << std::endl;
        std::cout << name() << ": Start solving" << std::endl;
    }

    solver->initialSolve();
    CbcModel model(*solver);
    model.setLogLevel(log_level - 1);
    model.setNumberThreads(8);
    CglFlowCover cut_flow;
    model.addCutGenerator(&cut_flow, 1, "FlowCover");
    CglMixedIntegerRounding2 cut_mir;
    model.addCutGenerator(&cut_mir, 1, "MIR");
    model.setAllowableGap(1e-10);
    CbcMain0(model);
    model.branchAndBound(1);
    ////////////////////
    const double * var_solution = model.bestSolution();
    if(var_solution == nullptr) {
        std::cerr << name() << ": Fail" << std::endl;
        delete solver;
        throw "caca";
    }
    for(const RestorationPlan<MutableLandscape>::Option i : plan.options()) {
        const int y_i = vars.y.id(i);
        double value = var_solution[y_i];
        solution.set(i, value);
    }
    solution.setComputeTimeMs(chrono.timeMs());
    solution.obj = model.getObjValue();
    solution.nb_vars = solver_builder.getNbNonZeroVars();
    solution.nb_constraints = solver_builder.getNbConstraints();
    solution.nb_elems = model.getNumElements();
    if(log_level >= 1) {
        std::cout << name()
                  << ": Complete solving : " << solution.getComputeTimeMs()
                  << " ms" << std::endl;
        std::cout << name() << ": ECA from obj : " << std::sqrt(solution.obj)
                  << std::endl;
    }
    delete solver;
    return solution;
#else
    const int nb_vars = solver_builder.getNbVars();
    double * objective = solver_builder.getObjective();
    double * col_lb = solver_builder.getColLB();
    double * col_ub = solver_builder.getColUB();
    char * vtype = new char[nb_vars];
    for(OSI_Builder::VarType * varType : solver_builder.getVarTypes()) {
        const int offset = varType->getOffset();
        const int last_id = varType->getOffset() + varType->getNumber() - 1;
        for(int i = offset; i <= last_id; i++) {
            vtype[i] = (!relaxed && varType->isInteger() ? GRB_BINARY
                                                         : GRB_CONTINUOUS);
        }
    }

    CoinPackedMatrix * matrix = solver_builder.getMatrix();
    const int nb_rows = matrix->getNumRows();
    const int nb_elems = matrix->getNumElements();
    int * begins = new int[nb_rows];
    std::copy(matrix->getVectorStarts(), matrix->getVectorStarts() + nb_rows,
              begins);
    int * indices = new int[nb_elems];
    std::copy(matrix->getIndices(), matrix->getIndices() + nb_elems, indices);
    double * elements = new double[nb_elems];
    std::copy(matrix->getElements(), matrix->getElements() + nb_elems,
              elements);
    double * row_lb = solver_builder.getRowLB();
    double * row_ub = solver_builder.getRowUB();

    GRBenv * env = NULL;
    GRBmodel * model = NULL;
    GRBemptyenv(&env);
    GRBstartenv(env);
    ////////////////////
    GRBsetdblparam(env, GRB_DBL_PAR_MIPGAP, 1e-8);
    GRBsetintparam(env, GRB_INT_PAR_LOGTOCONSOLE, (log_level >= 2 ? 1 : 0));
    GRBsetintparam(env, GRB_INT_PAR_THREADS, 8);
    GRBsetdblparam(env, GRB_DBL_PAR_TIMELIMIT, timeout);
    ////////////////////
    GRBnewmodel(env, &model, "pl_eca_2", 0, NULL, NULL, NULL, NULL, NULL);
    GRBaddvars(model, nb_vars, 0, NULL, NULL, NULL, objective, col_lb, col_ub,
               vtype, NULL);
    GRBaddrangeconstrs(model, nb_rows, nb_elems, begins, indices, elements,
                       row_lb, row_ub, NULL);
    ////////////////////
    GRBsetintattr(model, GRB_INT_ATTR_MODELSENSE, GRB_MAXIMIZE);

    if(log_level >= 1) {
        if(log_level >= 3) {
            name_variables(solver_builder, landscape, plan, preprocessed_datas,
                           vars);
            OsiClpSolverInterface * solver_clp =
                solver_builder.buildSolver<OsiClpSolverInterface>(
                    OSI_Builder::MAX);
            solver_clp->writeLp("pl_eca_2");
            delete solver_clp;
            std::cout << name() << ": LP printed to 'pl_eca_2.lp'" << std::endl;
        }
        std::cout << name() << ": Complete filling solver : "
                  << solver_builder.getNbConstraints() << " constraints and "
                  << solver_builder.getNbElems() << " entries in "
                  << chrono.lapTimeMs() << " ms" << std::endl;
        std::cout << name() << ": Start solving" << std::endl;
    }

    GRBoptimize(model);
    ////////////////////
    int status;
    GRBgetintattr(model, GRB_INT_ATTR_STATUS, &status);
    if(status == GRB_INF_OR_UNBD) {
        std::cout << "Model is infeasible or unbounded" << std::endl;
    } else if(status != GRB_OPTIMAL) {
        std::cout << "Optimization was stopped early" << std::endl;
    }
    double obj;
    GRBgetdblattr(model, GRB_DBL_ATTR_OBJVAL, &obj);
    double * var_solution = new double[nb_vars];
    GRBgetdblattrarray(model, GRB_DBL_ATTR_X, 0, nb_vars, var_solution);
    if(var_solution == nullptr) {
        std::cerr << name() << ": Fail" << std::endl;
        assert(false);
    }
    for(const RestorationPlan<MutableLandscape>::Option i : plan.options()) {
        const int y_i = vars.y.id(i);
        double value = var_solution[y_i];
        solution.set(i, value);
    }
    solution.setComputeTimeMs(chrono.timeMs());
    solution.obj = obj;
    solution.nb_vars = solver_builder.getNbNonZeroVars();
    solution.nb_constraints = solver_builder.getNbConstraints();
    solution.nb_elems = nb_elems;
    if(log_level >= 1) {
        std::cout << name()
                  << ": Complete solving : " << solution.getComputeTimeMs()
                  << " ms" << std::endl;
        std::cout << name() << ": ECA from obj : " << std::sqrt(solution.obj)
                  << std::endl;
    }

    GRBfreemodel(model);
    GRBfreeenv(env);

    delete[] vtype;
    delete[] begins;
    delete[] indices;
    delete[] elements;
    delete[] var_solution;

    return solution;
#endif
}

double Solvers::PL_ECA_2::eval(const MutableLandscape & landscape,
                               const RestorationPlan<MutableLandscape> & plan,
                               const double B,
                               const Solution & solution) const {
    const int log_level = params.at("log")->getInt();
    Chrono chrono;
    PreprocessedDatas preprocessed_datas(landscape, plan);
    OSI_Builder solver_builder = OSI_Builder();
    Variables vars(landscape, plan, preprocessed_datas);
    insert_variables(solver_builder, vars, preprocessed_datas);
    fill_solver(solver_builder, landscape, plan, B, vars, preprocessed_datas);
    for(const RestorationPlan<MutableLandscape>::Option i : plan.options()) {
        const int y_i = vars.y.id(i);
        double y_i_value = solution[i];
        solver_builder.setBounds(y_i, y_i_value, y_i_value);
    }
    OsiSolverInterface * solver =
        solver_builder.buildSolver<OsiClpSolverInterface>(OSI_Builder::MAX);
    ////////////////////
    solver->initialSolve();
    ////////////////////
    const double * var_solution = solver->getColSolution();
    if(var_solution == nullptr) {
        std::cerr << name() << ": Fail" << std::endl;
        delete solver;
        return 0.0;
    }
    double obj = solver->getObjValue();
    if(log_level >= 1) std::cout << name() << ": eval : " << obj << std::endl;
    delete solver;
    return obj;
}