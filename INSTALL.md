## Build tools

The build process requires CMake 3.12 or above (https://cmake.org/) and most of the dependencies are resolved with the Conan C++ package manager (https://conan.io/).

### Ubuntu
    sudo apt install cmake
    pip install conan
### Manjaro
    sudo pamac install cmake
    pip install conan

### Configure Conan for GCC >= 5.1
    conan profile update settings.compiler.libcxx=libstdc++11 default

## Dependencies
The project uses COIN-OR libraries such as LEMON for implementing graphs and Cbc as a default Mixed Integer Programming solver. Clp, Cbc and LEMON are not available in Conan so they need to be manually installed first.

## From package manager
### Ubuntu
    sudo apt install liblemon-dev
    sudo apt install coinor-libclp-dev coinor-libcbc-dev
### Manjaro (AUR)
    sudo pamac install coin-or-lemon
    sudo apt install coin-or-cbc

## From sources
### Lemon : http://lemon.cs.elte.hu/trac/lemon/wiki/Downloads
    wget http://lemon.cs.elte.hu/pub/sources/lemon-1.3.1.tar.gz
    tar -xf lemon-1.3.1.tar.gz
    cd lemon-1.3.1
    mkdir build
    cd build
    cmake ..
    make
    sudo make install
    
you can specify -DCMAKE_INSTALL_PREFIX=<install path> to cmake to customize the install directory, default is /user/local

### Cbc using coinbrew :
#### Ubuntu requirements
    sudo apt install build-essential git gcc-9 g++-9 cmake gfortran
#### Manjaro requirements
    pamac install base-devel git cmake gcc-fortran

#### Instructions
    mkdir coinor
    cd coinor
    git clone https://github.com/coin-or/coinbrew
    ./coinbrew/coinbrew fetch Cbc:releases/2.10.5
    ./coinbrew/coinbrew build Cbc:releases/2.10.5 --enable-cbc-parallel

then add to your .bashrc :

    export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:<path_to>/coinor/dist/lib/"

### Gurobi : https://www.gurobi.com/
    linux64/bin/grbgetkey <licence_key>
    cd linux64/src/build
    make
    mv libgurobi_c++.a ../../lib

then add to your .bashrc :

    export GUROBI_HOME="<path_to>/linux64"
    export PATH="$PATH:$GUROBI_HOME/bin"
    export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$GUROBI_HOME/lib"

