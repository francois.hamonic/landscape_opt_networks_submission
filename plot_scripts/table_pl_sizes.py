import matplotlib.pyplot as plt
import csv
import numpy as np


def readCSV(file_name, delimiter=','):
    file = csv.DictReader(open(file_name), delimiter=delimiter)
    return list([row for row in file])


def get_datas(rows, column_name):
    return [float(row[column_name]) for row in rows]


def avg(arr):
    if arr[0] != arr[0]:
        return "--"
    sum = 0
    cpt = 0
    for v in arr:
        if v == v:
            sum += v
            cpt = cpt + 1
    return int(sum / cpt / 10) / 100
    # return int(sum(arr) / len(arr) / 10) / 100


print("\\begin{tabular}{crrrrrrr}")

print("\t\\multirow{2}{*}{case} & \\multicolumn{3}{c}{MIP} & \\multicolumn{4}{c}{preprocessed MIP}\\tabularnewline")

print("\t\\cmidrule(lr){2-4}\\cmidrule(lr){5-8}")

print("\t& \\#var & \\#const & time & \\#var & \\#const & p. time & time \\tabularnewline")

print("\t\\hline")


# aude_rows = readCSV('output/time_aude.csv')
# print("\tAude & {} & {} & {} & {} & {} & {} & {}\\tabularnewline".format(int(get_datas(aude_rows, "pl_2_variables")[0]), int(get_datas(aude_rows, "pl_2_constraints")[0]), avg(get_datas(aude_rows, "pl_2_time")), int(
#     get_datas(aude_rows, "pl_3_variables")[0]), int(get_datas(aude_rows, "pl_3_constraints")[0]), avg(get_datas(aude_rows, "pl_3_preprocessing_time")), avg(get_datas(aude_rows, "pl_3_time"))))


quebec_rows = readCSV('output/time_quebec.csv')
print("\tMontereal & {} & {} & {} & {} & {} & {} & {}\\tabularnewline".format(int(get_datas(quebec_rows, "pl_2_variables")[0]), int(get_datas(quebec_rows, "pl_2_constraints")[0]), avg(get_datas(quebec_rows, "pl_2_time")), int(
    get_datas(quebec_rows, "pl_3_variables")[0]), int(get_datas(quebec_rows, "pl_3_constraints")[0]), avg(get_datas(quebec_rows, "pl_3_preprocessing_time")), avg(get_datas(quebec_rows, "pl_3_time"))))


# biorevaix_rows = readCSV('output/time_biorevaix.csv')
# print("\tAix & {} & {} & {} & {} & {} & {} & {}\\tabularnewline".format(int(get_datas(biorevaix_rows, "pl_2_variables")[0]), int(get_datas(biorevaix_rows, "pl_2_constraints")[0]), avg(get_datas(biorevaix_rows, "pl_2_time")), int(
#     get_datas(biorevaix_rows, "pl_3_variables")[0]), int(get_datas(biorevaix_rows, "pl_3_constraints")[0]), avg(get_datas(biorevaix_rows, "pl_3_preprocessing_time")), avg(get_datas(biorevaix_rows, "pl_3_time"))))


marseille_rows = readCSV('output/time_marseille.csv')
print("\tMarseille & {} & {} & {} & {} & {} & {} & {}\\tabularnewline".format(int(get_datas(marseille_rows, "pl_2_variables")[0]), int(get_datas(marseille_rows, "pl_2_constraints")[0]), avg(get_datas(marseille_rows, "pl_2_time")), int(
    get_datas(marseille_rows, "pl_3_variables")[0]), int(get_datas(marseille_rows, "pl_3_constraints")[0]), avg(get_datas(marseille_rows, "pl_3_preprocessing_time")), avg(get_datas(marseille_rows, "pl_3_time"))))


print("\\end{tabular}")
