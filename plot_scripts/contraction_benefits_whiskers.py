import matplotlib.pyplot as plt
import csv


def readCSV(file_name, delimiter=' '):
    file = csv.DictReader(open(file_name), delimiter=delimiter)
    return list([row for row in file])


rows = readCSV('output/contraction_benefits.csv', ",")

x_datas = range(0, 105, 5)


def get_datas(t, percent):
    return [100*(1-float(row["nb_{}_contract".format(t)])/float(row["nb_{}".format(t)])) for row in rows if int(row["percent_arcs"]) == percent]


vars_datas = [
    get_datas("vars", p) for p in x_datas
]
constr_datas = [
    get_datas("constraints", p) for p in x_datas
]
elems_datas = [
    get_datas("elems", p) for p in x_datas
]

plt.rcParams["figure.figsize"] = (10, 8)
plt.rcParams["font.size"] = 13.75

fig, axs = plt.subplots(3)

for i in [0, 1, 2]:
    # axs[i].axhline(y=75, color='lightblue', linestyle='--')
    # axs[i].axhline(y=50, color='dodgerblue', linestyle='--')
    # axs[i].axhline(y=25, color='lightblue', linestyle='--')
    axs[i].axhline(y=20, color='lightblue', linestyle='--')
    axs[i].axhline(y=40, color='lightblue', linestyle='--')
    axs[i].axhline(y=60, color='lightblue', linestyle='--')
    axs[i].axhline(y=80, color='lightblue', linestyle='--')

bplot1 = axs[0].boxplot(vars_datas, meanline=True, showmeans=True, whis=(0, 100), patch_artist=True)
axs[0].set(xlabel=None, ylabel='removed variables')
axs[0].set_xticklabels([])
bplot2 = axs[1].boxplot(constr_datas, meanline=True, showmeans=True, whis=(0, 100), patch_artist=True)
axs[1].set(xlabel=None, ylabel='removed constraints')
axs[1].set_xticklabels([])
bplot3 = axs[2].boxplot(elems_datas, meanline=True, showmeans=True, whis=(0, 100), patch_artist=True)
axs[2].set(xlabel="percentage of improvable arcs", ylabel='removed entries')
axs[2].set_xticklabels([x if x % 10 == 0 else "" for x in x_datas])

for bplot in (bplot1, bplot2, bplot3):
    for patch in bplot['boxes']:
        patch.set_facecolor('white')

plt.tight_layout()
fig.subplots_adjust(hspace=0.08)

plt.savefig("output/contraction_benefits_whiskers.pdf", dpi=500)
plt.show()
