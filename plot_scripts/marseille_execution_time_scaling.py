import matplotlib.pyplot as plt
import csv
import numpy as np
import statistics

def readCSV(file_name, delimiter=','):
    file = csv.DictReader(open(file_name), delimiter=delimiter)
    return list([row for row in file])

rows = readCSV('output/marseille_execution_time_scaling_tmp.csv')


x_datas = [int(float(row["wastelands"])) for row in rows]

def get_datas(name, linstyle, maker_size, t):
    return ((name, (linstyle,maker_size)), (x_datas,
        np.array([float(row[t])/1000 for row in rows]) ))

datas = [
    get_datas("local incremental", "s-",6, "local_inc"),
    get_datas("greedy decremental", "o-",6, "greedy_dec"),
    get_datas("MIP", "P-",6, "mip")
]


fig_size = plt.rcParams["figure.figsize"]
fig_size[0] = 10
fig_size[1] = 5
plt.rcParams["figure.figsize"] = fig_size

# plt.subplots_adjust(left=0.100, right=0.95, top=0.92, bottom=0.150)

plt.rcParams.update({'font.size': 16})

xmin = min(x_datas)
xmax = max(x_datas)

plt.xlim(xmin , xmax)

ymin = 0
ymax = max([float(row["mip"])/1000 for row in rows])
yrange = ymax - ymin

y_border_percent = 7.5
y_bottom = ymin - y_border_percent * yrange / 100
y_top = ymax + y_border_percent * yrange / 100

plt.ylim(y_bottom, y_top)


# plt.title("quebec-{}-{}-ECA value vs available budget.pdf".format(orig, median))
plt.ylabel('computation time in seconds', rotation=90, fontweight ='bold')
plt.xlabel("number of unbuilt lots", fontweight ='bold')

for ((label,(linestyle,marker_size)),(xdatas,ydatas)) in datas:
    plt.plot(xdatas, ydatas, linestyle, markersize=marker_size, label=label)
    
legend = plt.legend(loc='upper left', shadow=True, fontsize='medium')

plt.tight_layout()
plt.show()
plt.savefig("output/marseille_execution_time_scaling_tmp.pdf", dpi=500)