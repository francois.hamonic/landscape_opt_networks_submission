import matplotlib.pyplot as plt
import csv
import numpy as np
import statistics as stats

def readCSV(file_name, delimiter=','):
    file = csv.DictReader(open(file_name), delimiter=delimiter)
    return list([row for row in file])


def plot(name, file_name):
    rows = readCSV('output/{}'.format(file_name))

    def get_datas(solver_name):
        return [100 * float(row[solver_name]) / float(row['opt_delta_ECA']) for row in rows]

    IL = get_datas('naive_inc_delta_ECA')
    DL = get_datas('naive_dec_delta_ECA')
    IG = get_datas('glutton_inc_delta_ECA')
    DG = get_datas('glutton_dec_delta_ECA')

    print("{} & {:.3g}~\\% & {:.3g}~\\% & {:.3g}~\\% & {:.3g}~\\% & {:.3g}~\\% & {:.3g}~\\% & {:.3g}~\\% & {:.3g}~\\%\\tabularnewline\n\\hline".format(name, min(IL), stats.mean(IL), min(DL), stats.mean(DL), min(IG), stats.mean(IG), min(DG), stats.mean(DG)))


plot("Aude", "qos_aude.csv")
plot("Montreal", "qos_quebec.csv")
plot("Aix", "qos_biorevaix.csv")
plot("Marseille", "qos_marseille.csv")