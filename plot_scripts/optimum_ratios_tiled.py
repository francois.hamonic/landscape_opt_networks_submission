import matplotlib.pyplot as plt
import csv
import numpy as np

def readCSV(file_name, delimiter=','):
    file = csv.DictReader(open(file_name), delimiter=delimiter)
    return list([row for row in file])


def plot(name, subplot, file_name):
    rows = readCSV('output/{}'.format(file_name))

    max_delta = [float(row['max_delta_ECA']) for row in rows]

    def get_datas(solver_name):
        return ([float(row['budget_percent']) for row in rows], [float(row[solver_name]) / float(row['opt_delta_ECA']) for row in rows])
        # return ([float(row['budget_percent']) for row in rows], [(float(row[solver_name])  - float(row['bogo_avg_delta_ECA'])) / (float(row['opt_delta_ECA']) - float(row['bogo_avg_delta_ECA'])) for row in rows])

    datas = [
        (("static decremental", "v--"), get_datas('naive_dec_delta_ECA')),
        (("static incremental", "^--"), get_datas('naive_inc_delta_ECA')),
        (("decremental greedy", "v-"), get_datas('glutton_dec_delta_ECA')),
        (("incremental greedy", "^-"), get_datas('glutton_inc_delta_ECA')),
        (("optimal", "s-"), get_datas('opt_delta_ECA'))
    ]

    subplot.set_title(name)

    for ((label,linestyle),(xdatas,ydatas)) in datas:
        subplot.plot(xdatas, ydatas, linestyle, label=label, markersize=3.25)
        



fig, axs = plt.subplots(2, 2)
plot("Aude", axs[0, 0], "qos_aude.csv")
plot("Montreal", axs[0, 1], "qos_quebec.csv")
plot("BiorevAix", axs[1, 0], "qos_biorevaix.csv")
plot("Marseille", axs[1, 1], "qos_marseille.csv")

axs[0, 0].set(xlabel=None, ylabel='optimum ratio')
axs[0, 1].set(xlabel=None, ylabel=None)
axs[1, 0].set(xlabel='budget percent', ylabel='optimum ratio')
axs[1, 1].set(xlabel='budget percent', ylabel=None)

fig.subplots_adjust(bottom=0.3, wspace=0.2, hspace=0.35)
axs.flatten()[-2].legend(loc='upper center', bbox_to_anchor=(1.05, -0.35), ncol=3)

fig.set_size_inches(8,6)
plt.rcParams.update({'font.size': 18})

# plt.tight_layout()
plt.savefig("output/optimum_ratios.pdf", dpi=500)
# plt.show()