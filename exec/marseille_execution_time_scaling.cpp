#include <iostream>

#include "solvers/glutton_eca_dec.hpp"
#include "solvers/naive_eca_inc.hpp"
#include "solvers/pl_eca_3.hpp"

#include "instances_helper.hpp"
#include "print_helper.hpp"

int main() {
    std::ofstream data_log("output/marseille_execution_time_scaling.csv");
    data_log << std::fixed << std::setprecision(6);
    data_log << "wastelands,"
              << "local_inc,"
              << "greedy_dec,"
              << "mip" << std::endl;

    std::vector<int> nbs_wastelands;
    for(int i = 10; i <= 200; i += 10) nbs_wastelands.push_back(i);

    std::vector<int> seeds;
    for(int i = 0; i < 10; ++i) seeds.push_back(299 + i);

    std::vector<double> budget_percents;
    for(int i = 10; i < 90; i += 10) budget_percents.push_back(i);

    const int nb_tests = seeds.size() + budget_percents.size();

    Solvers::Naive_ECA_Inc naive_eca_inc;
    naive_eca_inc.setLogLevel(0).setParallel(true);
    Solvers::Glutton_ECA_Dec glutton_eca_dec;
    glutton_eca_dec.setLogLevel(0).setParallel(true);
    Solvers::PL_ECA_3 pl_eca_3;
    pl_eca_3.setLogLevel(0).setTimeout(3600);

    for(double nb_wastelands : nbs_wastelands) {
        double local_inc_time = 0;
        double greedy_dec_time = 0;
        double mip_time = 0;
        for(double seed : seeds) {
            Instance instance =
                make_instance_marseille(1, 0.135, 3000, nb_wastelands, seed);
            instance.plan.initElementIDs();
            const MutableLandscape & landscape = instance.landscape;
            const RestorationPlan<MutableLandscape> & plan = instance.plan;

            for(double budget_percent : budget_percents) {
                const double B = plan.totalCost() * budget_percent / 100;

                Solution naive_eca_inc_solution =
                    naive_eca_inc.solve(landscape, plan, B);
                Solution glutton_eca_dec_solution =
                    glutton_eca_dec.solve(landscape, plan, B);
                Solution pl_eca_3_solution = pl_eca_3.solve(landscape, plan, B);

                local_inc_time += naive_eca_inc_solution.getComputeTimeMs();
                greedy_dec_time += glutton_eca_dec_solution.getComputeTimeMs();
                mip_time += pl_eca_3_solution.getComputeTimeMs();
            }
        }

        local_inc_time /= nb_tests;
        greedy_dec_time /= nb_tests;
        mip_time /= nb_tests;

        data_log << nb_wastelands << "," << local_inc_time << ","
                  << greedy_dec_time << "," << mip_time << std::endl;
    }

    return EXIT_SUCCESS;
}